FROM tomcat:latest
RUN rm -rf /usr/local/tomcat/webapps/*
COPY target/ROOT.war /usr/local/tomcat/webapps/
CMD ["catalina.sh","run"]